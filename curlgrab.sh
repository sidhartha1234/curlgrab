#!/bin/bash
if [[ $# -eq 0 ]] ; then
	echo 'Usage: <hosts file>'
	echo 'Takes a list of hosts and uses curl to copy to index page of each. Each host is copied to its own html file.'
	exit 0
fi
echo "Creating Curled_Websites directory to store results"
mkdir Curled_Websites
while read host
do
	echo "Getting $host..."
	dir="Curled_Websites/"
	extension=".html"
	filename=$dir$host$html
	curl -s --fail --show-error --connect-timeout 5 "$host" >> $filename
done < "$1"

